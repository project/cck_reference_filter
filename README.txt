
DESCRIPTION
===========
The modules contained in this package enhance the user experience and ensure
data integrity when using the CCK submodules Node Reference and/or User 
Reference.
When linking a node to one or more nodes or users Node Reference Filter and
User Reference Filter limit the nodes or user names available to select from to
those that haven't been referenced yet from any other node.

Some example scenarios with one-to-one and one-to-many relationships whereby
the referenced nodes or users may be pointed to from one originating node only
are:
o a site with nodes of type meeting, each of which reference one agenda and
  one meeting minutes node; both agenda and minutes belong to that meeting only
o a site for the bridge card game where the site's registered users are the
  players; the players form teams (nodes) of two and each player can be in one
  team only
o a CMS containing a directory of companies and organisations, each
  referencing a contact person that is a registered user on the site; the
  user may only be a contact for at most one company or organisation
o a repository of financial reports whereby each company's financial annual
  report (node) references up to 12 monthly report nodes belonging to that
  company only
  
In general: any parent-child relationship whereby a parent may have multiple
children, but each child belongs to only one parent. In other words: child
selection is mutual exclusive.

Put another way, these modules make sure that the UI offers only 'orphan' 
nodes and users as valid destination references to select from, on the
originating node.

Enable Node Reference Filter for use with the CCK submodule Node Reference.
Enable User Reference Filter for use with the CCK submodule User Reference.
You also need to enable Content Reference Filter if you use either or both of
the other filter modules.

Both modules work with both the radio button and check box widgets of the 
Node and User Reference modules, as well as the single drop-down and multi-
select combo-boxes. 
They may be used in the standard and advanced (i.e. View) modes, as supplied
by CCK's Node Reference and User Reference modules.

The modules are a cinch to install. User Reference Filter has no configuration
options at all. Just enable and you're away. Node Reference Filter has a
couple of options, but the defaults are usually what you need. Enable the 
module and have a peek later at admin/settings/nodereference_filter for 
available options.
You configure node and user reference fields as before on the Content
management >> Manage fields >> Configure page of the content types in question.
Don't forget to tick the box "Ensure references are mutually exclusive" to 
activate this feature for the field in question.
As before users then select from the available references when they create or 
edit content of that type, but the options on offer will be a subset of the
original.

More information can be found in nodereference_filter/README.txt and
userreference_filter/README.txt.

FAQ
===
Q: What will happen when I run out of nodes (or users) to reference?
A: You'll get a message saying that all available nodes (users) have been
   taken. You must either relinquish an existing reference on another node
   or create more destination nodes (users).
   
Q: Can I potentially run into a deadlock when all references are taken
   and I cannot relinquish any, because when I created the reference field
   at Content management >> Manage fields I configured it to be "required",
   so it won't let me deselect it?
A: No problem, the modules understand looming deadlock. Say, you have a
   system with two "company" nodes and two registered users, each referenced
   from the company nodes through a company contact person reference field.
   You've realised that in linking the companies to the contacts you swapped
   the user names, which happen to be required fields. Because of this 
   you wouldn't normally be able to deselect your option. However the modules
   are clever in detecting required reference fields that have only one
   option remaining and will unlock the field for deselection by you. This way
   you can swap the references.
   
Q: If I have nodes or users that are referenced more than once at the
   time I enable the filter module(s), will second and subsequent references
   to these nodes and users be curtailed automatically to enforce mutual
   exclusion?
A: No, you'll have to do this yourself. The modules initially keep any
   existing selections. Auto-removal would not be a good idea as the modules
   can't read your mind as to which references are appropiate to clear.
   
Q: How do I find users that have been referenced, in particular referenced more
   than once?
A: No convenient solution to this. You could browse all the "My account" pages,
   i.e user/% pages (where % is the user id, or name if aliases have been set 
   up). Where referenced, the "My account" pages display links to the 
   referring nodes under the heading "Related content". For this section to
   appear you must tick the "Reverse links" checkbox on the Manage fields >> 
   Configure content admin page for the field in question.

Q: How do I find nodes that have been referenced, in particular referenced more
   than once?
A: The Node Reference Filter module comes with a View, named 
   'referenced_node_counts' that displays all nodes (i.e. titles) with their 
   reference counts, in descending order. You must have core's optional 
   Search module enabled. You must have run cron recently for an up to date
   report.
   With the canned view 'backlinks' enabled, referring nodes are also
   listed under the tab labelled 'What links here' (node/%/backlinks) on each
   node.

Q: Can I implement the functionality provided by these modules via a couple
   of advanced Views slotted into Node Reference and User Reference
   respectively?
A: Be my guest! The nature of the exclusion problem is such that it cannot
   easily be tackled with Views in particular with respect to already selected
   options on the form. Also, any Views solution is likely to be less performant
   than the dedicated code contained in this package.

Q: How come that unlike the other widgets, the auto-complete widget hasn't
   been adapted to operate in exclusion mode?
A: CCK's auto-complete widget does not cater for filtering of values in any way.
   hook_allowed_values() is not available for auto-complete.

Q: If I want to use Node Reference Filter for its Views only, do I still have
   to enable Content Reference Filter?
A: No, in this case you do not need Content Reference Filter.
