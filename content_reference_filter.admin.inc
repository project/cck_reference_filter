<?php

/**
 * @file
 * Admin configuration settings.
 */

/**
 * Menu callback for admin settings.
 */
function content_reference_filter_admin_configure() {
  $form['content_reference_filter_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display debug info regarding potential and excluded references.'),
    '#default_value' => variable_get('content_reference_filter_debug', variable_get('content_reference_filter_debug', FALSE)),
    '#description' => t('This information is displayed only to the admin user (uid=1) on the node creation and edit forms.')
  );
  return system_settings_form($form);
}