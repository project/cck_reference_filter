
DESCRIPTION
===========
Node Reference Filter is an extension to the CCK Node Reference module.
To ensure data integrity and improve the user experience when linking 
nodes this module makes sure that while selecting node references, users aren't
offered options that are already taken by other nodes.
Note that this does not affect the cardinality of the originating node. The
originating node may reference multiple nodes, just not any that are already
referenced from other nodes.

Examples of business cases and FAQs may be found in the README.txt in the 
module parent folder.

This module has two styles of interpreting "exclude nodes referenced from
any other node".
The default is to exclude nodes referenced from nodes that have a reference
field by the same name, analoguous to the User Reference Filter module.
The alternative option excludes nodes globally, that is: if any node of any
type references the node through any CCK field, then that node will be excluded.
See Administer >> Site configuration >> Node Reference Filter.

This module also provides a View field, "Node: Number of referring nodes",
with associated sort and filter. These may be used to compile Views of nodes
with their global link counts and to sort and filter on those counts. 
A built-in View is provided for immediate use or further customisation. It
provides an overview of all nodes that are linked to from other nodes, sorted
by their link counts. If you have the Views module installed, just visit
Site building >> Views >> List and find the view named 'referenced_node_counts'.

INSTALLATION
============
Uncompress cck_reference_filter.tgz into the sites/all/modules directory.
When you decided to use this module you would already have CCK installed with
the Node Reference submodule enabled.
Visit Administer >> Site building >> Modules,find the CCK section and tick the
boxes in front of both Content Reference Filter and Node Reference Filter.
Press "Save configuration".

CONFIGURATION
=============
As per normal, you add node reference fields at Administer >> Content 
Management >> Content types >> manage fields. 
The module works in both the standard and advanced (i.e. Views) modes and for
all option widgets except auto-complete.

For the global exclusion option you must enable the optional core module Search.
To keep core's linked nodes db table (aka {search_node_links}) up to date,
either the first or both of the following mechanisms are employed in the
background:
1) cron, via the OS, the poormanscron module or manual invocation
2) Node Reference Filter's auto-refresh: every time someone edits a node form
that has a node reference widget on it, i.e a select list or series of radio-
buttons or checkboxes.
The first mechanism is core's and is always in place so your list of nodes
not yet referenced will be accurate as of the most recent cron run.
The second mechanism is optional. It always produces up-to-date results but is
potentially slower on sites that have a lot of changing content. This feature 
may be switched on/off at Administer >> Site configuration >> Node Reference
Filter.
If you're not using the second mechanism, you can bring your site up to date by
invoking cron manually. Type either of the following in the address bar of your
browser:
o http://localhost/cron.php (does the job but leaves you with a blank page)
o http://localhost/admin/reports/status/run-cron (more elegant)

Or go to Administer >> Site configuration >> Search settings and press the 
button labeled "Re-index site" at the top of the page.

USAGE
=====
Users creating content (i.e a node) of a type that includes one or more node
references will find that the only nodes they're offered to link to are those
that aren't referenced already from another node (except when already selected
when this module was installed).

UNINSTALL
=========
The module does not alter the db schema or add any data to existing tables, so
it can be safely removed from your system without uninstalling. It does use
a couple of variables, so if you want to get rid of those, do it the proper
way: disable the module as per normal at Administer >> Site building >> 
Modules, save and click the Uninstall tab.
