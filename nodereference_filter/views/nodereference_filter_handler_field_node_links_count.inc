<?php

/**
 * @file
 *  Handler for the 'Node: Number of nodes linking to node'. field.
 */

class nodereference_filter_handler_field_node_links_count extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }

  function access() {
    return user_access('access content');
  }

  function query() {
    // Not calling parent::query() as it will treat 'links_count' as a real
    // db field, which it is not.
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $nid = $values->{$this->aliases['nid']};
    return nodereference_filter_global_links_count($nid);
  }
}
