<?php
/**
 * @file
 * Contains a default View to return an unformatted list of titles of nodes that
 * are currently not linked to. May be cloned and modified through the Views UI.
 */

/**
 * Implementation of hook_views_default_views().
 */
function nodereference_filter_views_default_views() {
  $view = new view;
  $view->name = 'referenced_node_counts';
  $view->description = "List of nodes with the number of times they're referenced from other nodes.";
  $view->tag = 'NodeReference Filter';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 3.0;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'links_count' => 'links_count',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'links_count' => array(
      'sortable' => 0,
      'align' => '',
      'separator' => '',
    ),
    'title' => array(
      'sortable' => 0,
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 0;
  $handler->display->display_options['style_options']['sticky'] = 0;
  /* Field: Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Node: Number of referring nodes. */
  $handler->display->display_options['fields']['links_count']['id'] = 'links_count';
  $handler->display->display_options['fields']['links_count']['table'] = 'node';
  $handler->display->display_options['fields']['links_count']['field'] = 'links_count';
  $handler->display->display_options['fields']['links_count']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['links_count']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['links_count']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['links_count']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['links_count']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['links_count']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['links_count']['alter']['html'] = 0;
  $handler->display->display_options['fields']['links_count']['hide_empty'] = 0;
  $handler->display->display_options['fields']['links_count']['empty_zero'] = 0;
  /* Sort criterion: Node: Number of referring nodes. */
  $handler->display->display_options['sorts']['links_count']['id'] = 'links_count';
  $handler->display->display_options['sorts']['links_count']['table'] = 'node';
  $handler->display->display_options['sorts']['links_count']['field'] = 'links_count';
  $handler->display->display_options['sorts']['links_count']['order'] = 'DESC';
  /* Sort criterion: Node: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';

  $views[$view->name] = $view;
  return $views;
}
