<?php

/**
 * @file
 *  Declare all Views handlers available in the nodereference_filter module.
 */

/**
 * @defgroup views handlers
 * @{
 */

/**
 * Implementation of hook_views_data_alter().
 */
function nodereference_filter_views_data_alter(&$data) {
  $data['node']['links_count'] = array(
    'title' => t('Number of referring nodes.'),
    'help' => t('Number of other nodes pointing to node.'),
    'field' => array(
      'handler' => 'nodereference_filter_handler_field_node_links_count',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'nodereference_filter_handler_filter_node_links_count'
    ),
    'sort' => array(
      'handler' => 'nodereference_filter_handler_sort_node_links_count'
    )
  );
}

/**
 * Implementation of hook_views_handlers().
 *
 * Register all of the handlers (fields, filters and sorts) introduced by this
 * module.
 */
function nodereference_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'nodereference_filter') .'/views',
    ),
    'handlers' => array(
      'nodereference_filter_handler_field_node_links_count'  => array('parent' => 'views_handler_field'),
      'nodereference_filter_handler_filter_node_links_count' => array('parent' => 'views_handler_filter_numeric'),
      'nodereference_filter_handler_sort_node_links_count'   => array('parent' => 'views_handler_sort'),
    )
  );
}
/**
 * @}
 */
