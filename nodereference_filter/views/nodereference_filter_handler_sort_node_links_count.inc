<?php

/**
 * @file
 * Contains Views content sort handler for Nodereference module.
 */

/**
 *
 * @ingroup views_sort_handlers
 */
class nodereference_filter_handler_sort_node_links_count extends views_handler_sort {

  /**
   * Called to add the sort to a query.
   *
   * NOTE: this filter cannot be used in a table sort.
   */
  function query() {
    $node_table = $this->ensure_my_table();
    $links_table = 'search_node_links'; // $this->query->ensure_table('search_node_links');
    $order_by_clause ='(SELECT COUNT(sid) FROM {'. $links_table .'} WHERE nid='. $node_table .'.nid)';
    $this->query->add_orderby(NULL, NULL, $this->options['order'], $order_by_clause);
  }
}
