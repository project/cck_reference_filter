<?php

/**
 * @file
 *   Views filter override to filter on the number of nodes linking to a node.
 *
 *   This filter is particularly useful with the Nodereference module to
 *   populate a drop-down with only those nodes that have not already been
 *   linked to from another node.';
 */
class nodereference_filter_handler_filter_node_links_count extends views_handler_filter_numeric {

  /**
   * Override introducing a pseudo-field to pass to the comparison
   * function in the query, e.g "... WHERE pseudo-field >= value"
   */
  function query() {
    if (empty($this->value)) {
      return;
    }
    $info = $this->operators();
    $method =  $info[$this->operator]['method'];
    if (!empty($method)) {
      $node_table = $this->ensure_my_table();
      $links_table = 'search_node_links'; // $this->query->ensure_table('search_node_links');
      $query = 'SELECT COUNT(sid) FROM {'. $links_table .'} WHERE nid='. $node_table .'.nid';
      $pseudo_field = "($query)";
      $this->$method($pseudo_field);
    }
  }
}

