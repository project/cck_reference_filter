<?php

/**
 * @file
 *  Extension for the Node Reference submodule, which is part of CCK.
 *  Improves the user experience when assigning node references by reducing
 *  the list of nodes available for linking to the subset of nodes that aren't
 *  already linked to other nodes.
 */

// Mutual exclusiong styles
define('NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION', 0); // default
define('NODEREFERENCE_FILTER_GLOBAL_EXCLUSION', 1);

/**
 * Implementation of hook_menu().
 */
function nodereference_filter_menu() {
  $items = array();
  $items['admin/settings/nodereference_filter'] = array(
    'title' => 'Node Reference Filter',
    'description' => 'Configure the scope of node reference exclusions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodereference_filter_admin_configure'),
    'access arguments' => array('administer site configuration'),
    'file' => 'nodereference_filter.admin.inc'
  );
  return $items;
}

/**
 * Implementation of hook_form_alter().
 * 
 * $form_ids of interest are 'CONTENTTYPE_node_form' eg 'meeting_node_form'.
 */
function nodereference_filter_form_alter(&$form, &$form_state, $form_id) {
  if (is_array($form['#field_info'])) {
    if (!module_exists('search')) {
      variable_set('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION);
    }
    if (variable_get('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION) != NODEREFERENCE_FILTER_GLOBAL_EXCLUSION) {
      variable_set('nodereference_filter_sync_linked_nodes', FALSE);
    }
    foreach($form['#field_info'] as $field_name => $field) {
      if (is_array($field) && is_array($field['widget']) && $field['widget']['mutually_exclusive_type'] && in_array($field['widget']['type'], array('nodereference_buttons', 'nodereference_select'))) {
        $sync_linked_nodes_data = variable_get('nodereference_filter_sync_linked_nodes', FALSE);
      }
    }
    if ($sync_linked_nodes_data) {
      // Reindexes nodes that were modefied recently, i.e. those that have 
      // reindex > 0 in the {search_dataset} table.
      // The maximum number of nodes that will be reindexed at a time comes from
      // the system variable 'search_cron_limit', as configured on the 
      // admin/settings/search page.
      node_update_index();
    }
  }
}

/**
 * Implementation of hook_allowed_values().
 * 
 * Called from the CCK submodule optionwidgets.module, this hook is called
 * when the form element attribute ['module'] is set to 'nodereference_filter',
 * as prepared by content_reference_filter_form_alter().
 *
 * This function tends to get called twice in a row, hence the statics to
 * cache the results of the database queries.
 */
function nodereference_filter_allowed_values($field) {
  static $references, $linked_nids;
  if (!isset($references)) {
    $references = _nodereference_potential_references($field);
  }
  if (!isset($linked_nids)) {
    if (variable_get('nodereference_filter_sync_linked_nodes', FALSE)) {
      $linked_nids = nodereference_filter_linked_nids_global();
    }
    else {
      $linked_nids = content_reference_filter_linked_ids($field['type_name'], $field['field_name'], 'nid', $field['multiple']);
    }
  }
  return content_reference_filter_assemble_options($field['field_name'], $references, $linked_nids);
}

/**
 * Return an array of the nids belonging to nodes that are linked to from 
 * any other nodes of any content type. 
 * Requires the Search module to be enabled and cron to have run recently.
 * 
 * @return array
 */
function nodereference_filter_linked_nids_global() {
  $linked_nids = array();
  $db_result = db_query('SELECT DISTINCT nid FROM {search_node_links}');
  while ($nid = db_result($db_result)) {
    $linked_nids[] = $nid;
  }
  return $linked_nids;
}

/**
 * Return the number of nodes that point to the node with the supplied id.
 * Requires the Search module to be enabled and the site reindexed recently,
 * e.g via cron.
 * @param $nid
 * @return count (int)
 */
function nodereference_filter_global_links_count($nid) {
  return db_result(db_query('SELECT COUNT(sid) FROM {search_node_links} WHERE nid=%d', $nid));
}

/**
 * Implementation of hook_views_api().
 */
function nodereference_filter_views_api() {
  return array(
    'api' => views_api_version(),
    'path' => drupal_get_path('module', 'nodereference_filter') .'/views'
  );
}
