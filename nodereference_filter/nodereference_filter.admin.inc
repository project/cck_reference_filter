<?php

/**
 * @file
 * Admin configuration settings.
 */

/**
 * Menu callback for admin settings.
 */
function nodereference_filter_admin_configure() {
  $form['nodereference_filter_exclusion'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure the scope of node reference exclusions'),
    '#description' => t('For CCK fields that use the node reference widget decide in what way the widget will exclude nodes that are already referenced from other nodes.')
  );
  if (module_exists('search')) {
    $form['nodereference_filter_exclusion']['nodereference_filter_exclusion_style'] = array(
      '#type' => 'radios',
      '#title' => t('Mutual exclusion style'),
      '#options' => array(
        NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION => t('for the node reference field in question only'),
        NODEREFERENCE_FILTER_GLOBAL_EXCLUSION  => t('global, across all node reference fields in all content types'),
      ),
      '#default_value' => variable_get('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION),
      '#description' => t('The first option is the default and is what is also used by the User Reference module.')
    );
    $form['nodereference_filter_exclusion']['nodereference_filter_sync_linked_nodes'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ensure <em>linked nodes</em> data is always up to date.'),
      '#default_value' => variable_get('nodereference_filter_sync_linked_nodes', variable_get('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION) == NODEREFERENCE_FILTER_GLOBAL_EXCLUSION),
      '#description' => t("Applies to the above global exclusion option only. If this box is ticked then the system refreshes, if necessary, the <em>linked nodes</em> data prior to populating any node reference form field. This means that the presented options are guarateed to be up to date. On sites with a lot of content that is frequently updated, this may be a CPU-intensive (i.e. slow) operation. In this case you may leave the box unticked so that there is no overhead as the system will use the <em>linked nodes</em> information as updated during the last cron run. See %link for more information.", array('%link' => '/admin/settings/search'))
    );
  }
  else {
    if (variable_get('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION) == NODEREFERENCE_FILTER_GLOBAL_EXCLUSION) {
      variable_set('nodereference_filter_exclusion_style', NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION);
      variable_set('nodereference_filter_sync_linked_nodes', FALSE);
    }
    $form['nodereference_filter_exclusion']['nodereference_filter_exclusion_style'] = array(
      '#type' => 'radios',
      '#title' => t('Decide how mutual exclusion of referenced nodes is to be applied'),
      '#options' => array(
        NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION => t('for the node reference field in question only'),
      ),
      '#default_value' => NODEREFERENCE_FILTER_PER_FIELD_EXCLUSION,
      '#description' => t('As you do not have the <em>Search</em> module (core - optional) enabled, the option to exclude node references globally is currently not availalbe. Please enable <em>Search</em> at %link if you wish to employ this option.', array('%link' => 'admin/build/modules'))
    );
  }
  return system_settings_form($form);
}