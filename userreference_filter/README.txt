
DESCRIPTION
===========
User Reference Filter is an extension to the CCK User Reference module.
To ensure data integrity and improve the UI when linking nodes to users, 
this module makes sure that while selecting user references, no user names
are offered that are already referenced from other nodes.
Note that this does not affect the cardinality of the originating node. The
originating node may reference multiple users, just not any that are already
referenced.

Examples of business cases and FAQs may be found in the README.txt in the 
module parent folder.

INSTALLATION
============
Uncompress cck_reference_filter.tgz into the sites/all/modules directory.
If you're considering using this module, you will already have CCK installed
with the User Reference submodule enabled.
Visit Administer >> Site building >> Modules,find the CCK section and tick the
boxes in front of both Content Reference Filter and User Reference Filter.
Press "Save configuration".

CONFIGURATION
=============
None for this module. 
As per normal, you add user reference fields at Administer >> Content Management
>> Content types >> manage fields. 
The module works in both the standard and advanced (i.e. Views) modes and for
all option widgets except auto-complete.

USAGE
=====
Users creating or editing nodes of a type that includes one or more user
references will find that the only user names they're offered to link to are 
those that aren't referenced already by a another node via the same
reference field (except when already selected when this module was enabled).

UNINSTALL
=========
Nothing to do. The module does not alter the db schema or add any data to
existing tables, so it can be safely removed from your system without 
uninstalling.
 